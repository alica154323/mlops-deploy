import mlflow


def load_model():
    model_name = "classifier"
    model_version = "latest"
    model_uri = f"models:/{model_name}/{model_version}"
    model = mlflow.pyfunc.load_model(model_uri)
    return model
