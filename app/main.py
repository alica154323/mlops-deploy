import pandas as pd
from fastapi import Depends, FastAPI
from fastapi.security import HTTPBearer
from prometheus_fastapi_instrumentator import Instrumentator

from auth import bearer_auth
from preprocessing import preprocess
from schemas import RequestBody
from utils import load_model

model = load_model()
app = FastAPI()
Instrumentator().instrument(app).expose(app)

security = HTTPBearer()


@app.post("/", dependencies=[Depends(bearer_auth)])
async def predict(body: RequestBody):
    data = body.dict()
    data = preprocess(data)
    data = pd.DataFrame(data, index=[0])
    result = model.predict(data)[0]
    return "Yes" if result == 1 else "No"
