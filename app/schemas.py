from pydantic import BaseModel


class RequestBody(BaseModel):
    Age: int
    Gender: str
    family_history: str
    benefits: str
    care_options: str
    anonymity: str
    leave: str
    work_interfere: str
