def preprocess(data: dict):
    data["Gender"] = _clean_gender(data["Gender"])
    return data


def _clean_gender(gender: str) -> str:
    male_str = [
        "male",
        "m",
        "male-ish",
        "maile",
        "mal",
        "male (cis)",
        "make",
        "male ",
        "man",
        "msle",
        "mail",
        "malr",
        "cis man",
        "Cis Male",
        "cis male",
    ]
    trans_str = [
        "trans-female",
        "something kinda male?",
        "queer/she/they",
        "non-binary",
        "nah",
        "all",
        "enby",
        "fluid",
        "genderqueer",
        "androgyne",
        "agender",
        "male leaning androgynous",
        "guy (-ish) ^_^",
        "trans woman",
        "neuter",
        "female (trans)",
        "queer",
        "ostensibly male, unsure what that really means",
    ]
    female_str = [
        "cis female",
        "f",
        "female",
        "woman",
        "femake",
        "female ",
        "cis-female/femme",
        "female (cis)",
        "femail",
    ]

    gender_map = {gender: "male" for gender in male_str}
    gender_map.update({gender: "female" for gender in female_str})
    gender_map.update({gender: "trans" for gender in trans_str})
    gender = gender_map.get(gender.lower(), "")
    return gender
