from os import getenv

from fastapi import Request
from fastapi.exceptions import HTTPException
from fastapi.security import HTTPAuthorizationCredentials, HTTPBearer


class SimpleBearerAuth(HTTPBearer):
    def __init__(self, auto_error: bool = True):
        super().__init__(auto_error=auto_error)

    def _validate_token(self, token: str) -> bool:
        return token == getenv("TOKEN")

    async def __call__(self, request: Request):
        credentials: HTTPAuthorizationCredentials | None = await super().__call__(request)
        if credentials:
            if not credentials.scheme == "Bearer":
                raise HTTPException(status_code=401, detail="Invalid authentication scheme")
            if not self._validate_token(credentials.credentials):
                raise HTTPException(status_code=401, detail="Invalid authentication scheme")
            return credentials.credentials

        raise HTTPException(status_code=401, detail="Invalid authentication scheme")


bearer_auth = SimpleBearerAuth()
